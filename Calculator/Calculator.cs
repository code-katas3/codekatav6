﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculators
{
    public class Calculator
    {
        string newLineDelimiter = "\n";
        string forwardSlashDelimiter = "//";
        string commaDelimiter = ",";
        string squareBracketDelimiter = "][";
        string openingBracket = "[";

        public int Add(string numbers)
        {
            var strings = sortDelimiters(numbers);
            var numberList = new List<int>();
            foreach (var s in strings)
            {
                if (int.TryParse(s, out int number))
                {
                    numberList.Add(number);
                }
            }
            getInvalidChecks(numberList);
            return numberList.Sum();
        }

        private string[] GetDelimiters(string numbers)
        {
            if (!numbers.StartsWith(forwardSlashDelimiter))
            {
                return new string[] { commaDelimiter, newLineDelimiter };
            }
            int delimiterLength = numbers.IndexOf(newLineDelimiter) - 2;
            var delimiterData = numbers.Substring(2, delimiterLength);
            if (delimiterData.Contains(openingBracket))
            { 
                delimiterData = delimiterData.Substring(1, delimiterData.Length - 2);
                return delimiterData.Split(new string[] { squareBracketDelimiter }, StringSplitOptions.RemoveEmptyEntries);
            }
            var delimiterString = numbers.Substring(2, 1);
            return new string[] { delimiterString };
        }

        private void negativeNumberCheck(List<int> numberList)
        {
            var negativeNumbers = numberList.Where(x => x < 0).ToList();
            if (negativeNumbers.Any())
            {
                throw new Exception("Negatives not allowed : " + string.Join(commaDelimiter, negativeNumbers));
            }
        }
         
        private void IgnoreNumbersGreaterThan1000(List<int> numberList)
        {
            var maxNumber = 1000;
            numberList.RemoveAll(x => x > maxNumber);
        }

        private string removeDelimiterData(string numbers)
        {
            if (numbers.StartsWith(forwardSlashDelimiter))
            {
                int startDelimiter = numbers.IndexOf(newLineDelimiter) + 1;
                return numbers.Substring(startDelimiter);
            }
            return numbers;
        }

        private string[] sortDelimiters(string numbers)
        {
            var delimiters = GetDelimiters(numbers);
            numbers = removeDelimiterData(numbers);
            var strings = numbers.Split(delimiters, StringSplitOptions.None);
            return strings;       
        }

        private void getInvalidChecks(List<int> numberList)
        {
            negativeNumberCheck(numberList);
            IgnoreNumbersGreaterThan1000(numberList);
        }
    }
}
